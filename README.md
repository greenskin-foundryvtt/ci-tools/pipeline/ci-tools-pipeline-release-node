# ci-tools-pipeline-release-tools

This module contains abstract gitlab ci function for release node project

## release-node.yml

Since 1.0.0

Abstract ci function to release node project based on npm

### Dependencies

- release-common
- a package.json file with version
- run into an image with node and yarn

### Usage

- declare a stages prepare_release and release
- create a job prepare_release extends .prepare_release_node and with your own rules
- create 3 manual job and extends the corresponding release type from release-node


```
include:
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release'
    ref: 1.0.0
    file: '/release-common.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release-node'
    ref: 1.0.0
    file: '/release-node.yml'

stages:
  - prepare_release
  - release
  
prepare_release:
  extends: .prepare_release_node
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: on_success
      
release:patch:
  extends: .release-node:patch
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual

release:minor:
  extends: .release-node:minor
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual

release:major:
  extends: .release-node:major
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual
```
